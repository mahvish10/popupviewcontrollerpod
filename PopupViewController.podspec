Pod::Spec.new do |s|
   s.name             = 'PopupViewController'
   s.version          = '0.1.0'
   s.summary          = 'Create indepenedent PopupViewController.'
   s.homepage         = 'https://gitlab.com/mahvish10/PopupViewController'
   s.license          = { :type => 'MIT', :file => 'LICENSE' }
   s.author           = { 'mahvish10' => 'mahvishsyed15@gmail.com' }
   s.source           = { :git => 'https://gitlab.com/mahvish10/PopupViewController.git', :tag => s.version.to_s }
   s.ios.deployment_target = '14.0'
   s.swift_version = '4.2'
   s.source_files = 'PopupViewController/Classes/**/*'
   s.dependency 'Generics'
end
